package com.appstuds.criclive.Interface;

/**
 * Created by osr on 9/1/18.
 */

public interface ItemClick {
    public void clickAction(String url);
    public void passShare(String share);
    public void passDisc(String Disclaimer);
}
