package com.appstuds.criclive.Activities;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.appstuds.criclive.R;

public class VideoActivity extends AppCompatActivity {

    WebView mywebview;
    private ProgressBar mProgressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        mProgressBar = (ProgressBar) findViewById(R.id.pb);
        String url = getIntent().getExtras().getString("Url");
        mywebview = (WebView) findViewById(R.id.webView1);
        WebSettings webSettings = mywebview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mywebview.setWebViewClient(new WebViewClient());
        mywebview.loadUrl(url);

    }

//        mywebview.setWebViewClient(new WebViewClient(){
//
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                super.onPageStarted(view, url, favicon);
//
//                mProgressBar.setVisibility(View.INVISIBLE);
//            }
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                WebSettings webSettings = mywebview.getSettings();
//                webSettings.setJavaScriptEnabled(true);
//                mywebview.loadUrl(url);
//                return true;
//            }
//        });


      //  mywebview.loadUrl(url);


    public class WebViewClient extends android.webkit.WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageFinished(WebView view, String url) {

            super.onPageFinished(view, url);
            mProgressBar.setVisibility(View.GONE);

        }

    }



}
