package com.appstuds.criclive.Fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.appstuds.criclive.Adapters.CustomSwipeAdapter;
import com.appstuds.criclive.Adapters.RecyclerAdapter;
import com.appstuds.criclive.Interface.Api;
import com.appstuds.criclive.Modal.Feed;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.appstuds.criclive.Modal.Matches;
import com.appstuds.criclive.Modal.Slider;
import com.appstuds.criclive.R;
import com.viewpagerindicator.LinePageIndicator;





public class HomeFragment extends Fragment {
    View root;
    RecyclerView recyclerView;
    private List<Matches> datalist;
    private List<Slider> sliderlist;
    private String Disclaimer;
    private String Share;

    private RecyclerView.Adapter adapter;
    public LinearLayoutManager layoutManager;
    ViewPager viewPager;
    CustomSwipeAdapter customSwipeAdapter;
    LinePageIndicator mIndicator;
    int currentPage=0;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView=(RecyclerView)root.findViewById(R.id.recycler_view);

        datalist = new ArrayList<>();
        sliderlist=new ArrayList<>();


        viewPager=(ViewPager)root.findViewById(R.id.view_pager);
      mIndicator = (LinePageIndicator) root.findViewById(R.id.indicator);




        getResults();
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);

    }





    private void getResults() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        Api api = retrofit.create(Api.class);

        Call<Feed> call = api.getResults();

        call.enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {
                 datalist = response.body().getResult().getMatches();
                 sliderlist=response.body().getResult().getSlider();
                 Disclaimer=response.body().getResult().getDisclaimer();
                 Share=response.body().getResult().getInvitation().getAndroid();




                layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                adapter = new RecyclerAdapter(getActivity(), datalist);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(adapter);
                customSwipeAdapter=new CustomSwipeAdapter(getActivity(),sliderlist,Share,Disclaimer);
                viewPager.setAdapter(customSwipeAdapter);
                setbanner();
                mIndicator.setViewPager(viewPager);



            }


            public void setbanner()
            {
               final Handler handler = new Handler();

              final Runnable update = new Runnable() {
                    public void run() {
                        if (currentPage == sliderlist.size()) {
                            currentPage = 0;
                        }
                        viewPager.setCurrentItem(currentPage++, true);
                    }
                };


                new Timer().schedule(new TimerTask() {

                    @Override
                    public void run() {
                        handler.post(update);
                    }
                }, 1000, 3000);

            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }





}
