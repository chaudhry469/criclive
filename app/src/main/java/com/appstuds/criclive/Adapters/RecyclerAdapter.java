package com.appstuds.criclive.Adapters;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appstuds.criclive.Activities.VideoActivity;
import com.appstuds.criclive.Modal.Matches;
import com.appstuds.criclive.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cn.iwgang.countdownview.CountdownView;

import static android.content.ContentValues.TAG;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

   Context context;

    View view;
    SimpleDateFormat sdf;


    private List<Matches> my_data;


    public RecyclerAdapter(Context context, List<Matches> my_data)
    {

      this.context=context;

        this.my_data=my_data;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         view= LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_adapterdata,null);

        RecyclerViewHolder recyclerViewHolder=new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.team1name.setText(my_data.get(position).getTeam1().getShort_name());
        holder.team2name.setText(my_data.get(position).getTeam2().getShort_name());


        Picasso.with(context) .load(my_data.get(position).getTeam1().getLogo())
                .into(holder.team1flag);
        Picasso.with(context) .load(my_data.get(position).getTeam2().getLogo())
                .into(holder.team2flag);

       String THorRD=getTHorRD(my_data.get(position).getMatch_number());

       String TimestToDate=getTStoDate(my_data.get(position).getMatch_date());

       int imageribbon=ribbon(my_data.get(position).getMatch_type().toUpperCase());

       holder.ribbon.setImageResource(imageribbon);


         sdf= new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");

        Date date=null;
        try {
             date = sdf.parse(TimestToDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date now = new Date();
        String strDate = sdf.format(now);//(use it)

        long diffInDays=getDuration(strDate,TimestToDate);



        holder.match_type.setText(my_data.get(position).getMatch_number()+THorRD+my_data.get(position).getMatch_type().toUpperCase()+" MATCH");
        holder.match_date.setText(outputFormat.format(date));

       // holder.live_match_url.setText(my_data.get(position).getLive_match_url());
      //  holder.live_match_url.setText(my_data.get(position).getBroadcast_message());

        if((my_data.get(position).getLive_match_url())!="" &&(my_data.get(position).getLive_match_url())!=null)
        {

            holder.live_match_url_textView.setVisibility(View.INVISIBLE);
            holder.live_match_button.setVisibility(View.VISIBLE);
        }
        else if ((my_data.get(position).getBroadcast_message())!="")
        {
            holder.live_match_url_textView.setText(my_data.get(position).getBroadcast_message());

        }
        else if ((my_data.get(position).getLive_match_url()).equals("")&& (my_data.get(position).getBroadcast_message()).equals(""))
        {
           // holder.live_match_url_textView.setVisibility(View.VISIBLE);
          //  holder.live_match_button.setVisibility(View.INVISIBLE);
           // holder.live_match_url_textView.setText("timer");
            holder.countdownView.setVisibility(View.VISIBLE);
            holder.countdownView.start(diffInDays);
        }

      //  Log.d("disc",my_data.get(position).)







        holder.live_match_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // holder.live_match_url.setText(my_data.get(position).getLive_match_url());
                Intent intent=new Intent(context,VideoActivity.class);
                intent.putExtra("Url",my_data.get(position).getLive_match_url());
                context.startActivity(intent);






            }
        });







    }

    @Override
    public int getItemCount() {

        return my_data.size();

    }

    public String getTHorRD(String value)
    {
        int valueint=Integer.parseInt(value);
        if(valueint==1) return "ST ";
        else if(valueint==2) return "ND ";
        else if(valueint==3) return "RD ";
        else return "TH ";

    }

    public String getTStoDate(String tsValue)
    {


        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        Long num=Long.parseLong(tsValue);
        cal.setTimeInMillis(num*1000);
        String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
        return date;

    }

    public long getDuration(String strDate,String TimestToDate)
    {
        java.util.Date d = null;
        java.util.Date d1 = null;

        try {
            d = sdf.parse(TimestToDate);
            d1 = sdf.parse(strDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        long difff= (d.getTime() - d1.getTime());

//
//        float days= difff/(60 * 60 * 1000 * 24);
//
//        int days1=(int)days;
//
//        float result=days-days1;
//
//
//        float hours=result*24;
//        int hours1=(int)hours;
//
//        float result1=hours-hours1;
//
//        float min=result1*60;
//
//
//
//        Log.d("heyyy",min+"");
//
//
//        long days1=Integer.parseInt((difff/(60 * 60 * 1000 * 24))+"");
//        long seconds = TimeUnit.MILLISECONDS.toSeconds(difff);
//        long minutes = TimeUnit.MILLISECONDS.toMinutes(difff);


        return difff ;

    }

    public int ribbon(String value)
    {
         int[] image_resources = {R.drawable.t20_ribbon,
                 R.drawable.odi_ribbon,R.drawable.test_ribbon};

         if (value.equals("T20")) return image_resources[0];
         else if(value.equals("ODI")) return image_resources[1];
         else if (value.equals("TEST")) return  image_resources[2];
         else return 1;






    }


    public  class RecyclerViewHolder extends RecyclerView.ViewHolder
    {
           TextView team1name,team2name,match_number,match_type,match_date,live_match_url_textView;//,release_date,ratingxml,moreInfoXml,secondapi;
           ImageView team1flag,team2flag,ribbon;
           Button live_match_button;
        CountdownView countdownView;


        public RecyclerViewHolder(View View) {
            super(View);
            team1flag=(ImageView) View.findViewById(R.id.team1_flag);
            team2flag=(ImageView) View.findViewById(R.id.team2_flag);
            team1name=(TextView)View.findViewById(R.id.team1_name);
            team2name=(TextView)View.findViewById(R.id.team2_name);
            match_type=(TextView)View.findViewById(R.id.match_no);//change it
            match_date=(TextView)View.findViewById(R.id.match_date);
            live_match_url_textView=(TextView)View.findViewById(R.id.watchnow_startin);
            live_match_button=(Button)View.findViewById(R.id.buttoncomego);
            countdownView=(CountdownView) View.findViewById(R.id.countdown_view);
            ribbon=(ImageView)View.findViewById(R.id.ribbon);




        }
    }




}
